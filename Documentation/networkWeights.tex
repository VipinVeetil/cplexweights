\documentclass[12pt]{article}
\usepackage[margin=1.0in]{geometry}
\usepackage{amsmath}
\usepackage{enumerate}
\usepackage{setspace}
\usepackage[dvipsnames]{xcolor}
%\onehalfspacing
%Gummi|065|=)
\title{\textbf{Estimating weights of an economic network}}
\author{Antoine Mandel \\
		Vipin P. Veetil}
%\date{}
\begin{document}

\maketitle

\section{Introduction}
This document explains how to estimate the weights of an economic network of a model developed by Gauldi and Mandel. 
\section{The problem}
The economy consists of a representative household indexed by $0$, and the firms in the economy be indexed by $i \in (1, ..., n)$.  Adjacency matrix $A$ characterizes the network of the economy with $a_{ij}$ indicating the flow of money from $i$ to $j$ and the flow of goods in the opposite direction. The dimension of matrix $A$ is therfore $(n+1) \times (n+1)$.  Let vector $m$ denote the distribution of money among agents in the economy, therefore the dimension of $m$ is $(n+1) \times 1$. The problem is to compute the non-zero weights in $A$ such that it yields an invariant distribution of money: $A^T m = m$. Furthermore, we'd like the household's spending on different firms meet a sectoral distribution of household spending, for example the sum of household spending on firms in the services sectors must be the aggregate share of household spending on services. Let $s\in S$ denote a sector in the economy, where $S$ is the set of all sectors. If a firm $i$ is in sector $s$, then we say $i\in s$.  Note that agents are free to buy goods from themselves: $a_{ii}\geq0$. We shall call  $a_{ii}$ ``self weights''. Let the households share of spending on sector $s$ as a proportion of all spending not on itself be denoted by $h_s$.  With that said,  the optimization problem can be written specified as: 
\begin{equation}
\begin{aligned}
& \underset{x}{\text{minimize}}
&  F(A) \\
& \text{subject to Constraint I}
& Am = m \\
& \text{subject to Constraint II}
&  \sum_{j=0}^{n} a_{ij} = 1, \forall i \in (0,\ldots,n) \\ 
& \text{subject to Constraint III}
&  (1-a_{00})(\sum_{j\in s} a_{0j}) = h_s, \forall s \in S \\ 
& \text{with  }  0 \leq a_{ij}  \leq 1, \forall  a_{ij} \in A
\end{aligned}
\end{equation}
We shall specify an objective function $F(A)$ with two goals in mind. The first goal is the ensure the economic network is preserved in the process of estimation of its weights. In other words, we would not like the non-zero entries of $A$ to be set to zero in the process of optimization. This goal is achieved by minimizing the sum of the square of weights $a_{ij}$. The second goal is to minimize the flow of money from an agent to itself. In principle agents are free to buy goods from themselves: $a_{ii}\geq0$. We shall call  $a_{ii}$ ``self weights''. Non zero self weights are needed because an invariant money distribution may not exist in the absence of self weights. However, we would like to minimize self weights so as to have the greatest possible flow of money between agents in the network. The goal is achieved by minimizing the sum of self weights. More specifically, the objective function is:
\begin{equation}
 F(A) = \sum_{i,j \in (0,\ldots,n)} a_{ij}^2 + a_{ii}
\end{equation}


\section{Entering the problem into IBM's Cplex}
The problem is solved using  Cplex Python API. Cplex is a state of the art optimization software developed by of IBM. It is freely available to academic users. Note that the problem characterized above is a quadratic programming problem, with the unknowns in the matrix $A$ and the knowns are in vector $m$. In some parts of the literature such problems are known as matrix quadratic programming problem. Cplex takes quadratic programming problems in the following form:
\begin{equation}
\begin{aligned}
 & \underset{x}{\text{minimize}}
&  F(A) =  x^TQx  + C^Tx
\end{aligned}
\end{equation}
where $x$ is the vector of unknowns,  the matrix $Q$ specifies the quadratic part of the problem, and the vector $C$ specifies the linear part of the problem. At this point it is worth noting the dimensions of the matrix and vectors in the problem.
\\
\indent
$A$ in the original problem is $(n+1)\times(n+1)$. Let the number of unknowns, i.e. the non-zero entries of matrix $A$ be denoted by $k$.  Zero entries indicate that there is no buyer-seller relation between two agents, and these are ``known'' from the network data. In vector $x$ we shall include only the non-zero entries of $A$. Therefore the dimension of vector $x$ is $k\times1$,  the dimension of Q is $k \times k$, and the dimension of C is $k \times 1$.
\\
\indent 
The two constraints of the original problem too must be converted into formats in Cplex accepts problems. The first constraint $Am=m$ specifies the unknowns in matrix $A$ and the knowns in vector $m$.  Note that $m$ is  a $(n+1) \times 1$ vector of the distribution of money among agents. The Cplex format is to specify the unknows in a vector and the knowns in matrix. Therefore $Am$ needs to be converted to $Mx$, where x is the $k \times 1$ vector of unknown weights as before, and $M$ is a $(n+1) \times k$ matrix whose entries are either 0 or elements of the vector of money distribution $m$.
\\
\indent
The second constraint $ \sum_{j=0}^{n} a_{ij} = 1, \forall i \in (0,\ldots,n)$ needs to be written in matrix form. More specifically, $Ex=e$ characterizes the constraint, where $x$ is a $k\times1$ vector of unknowns as before, $E$ is a $n\times k$ matrix whose entries are either 0 or 1, and $e$ is a $n \times 1$ vector whose entries are 1. 
\\
\indent
The third constraint $ (1-a_{00})(\sum_{j\in s} a_{0j}) = h_s, \forall s \in S$ needs to written in matrix form. More specifically, $Hx=h$ characterizes the constraint, where $x$ is a $k\times1$ vector of unknowns as before, $H$ is a $r\times k$ matrix whose entries are either 0 or 1, and $h$ is a $r \times 1$ vector of the shares of household spending to different sectors of the economy, where $r$ is the number of sectors in the economy.
\\
\indent
And finally, the lower and upper bounds of the weights $a_{ij} \in A$ or equivalently non-zero $a_{ij} \in x$ need to be specified in Cplex. The upper bounds are set to 1 for all elements in vector x.  The lower bounds are set using a parameter $\lambda$. More specificially,  the lower bound of $a_{ij} \forall j$ is $b_i=\frac{1}{\lambda d_i}$, where $d_i$ is the number of outflows of money from agent $i$ or equivalently the number of sellers of inputs to agent $i$. Where $\lambda \in (1, \infty)$, when $\lambda=1$ the lower bound is such that an agent spends equally on all its suppliers of inputs, as $\lambda$ tends to $\infty$ the lower bound goes to zero.
 


\section{The data}
The code takes four files of data to solve the optimization problem. One, a file containing information on the buyer-seller linkages between firms called \emph{network.txt}. Two, a file containing information on the sector to which each firm belongs called \emph{firmSector.txt}. Three, a file containing information on the shares of household expenditure on different sectors called \emph{hhShares.txt}. Four, a file containing information on the distribution of money among firms in the economy called \emph{money.txt}.
The following subsections specify the format of the files.
\subsection{Firm network as \emph{network.txt}}
This file contains only information on the what each firm buys from \emph{other} firms.
\begin{enumerate}[I]
\itemsep-0.25em
\item Each line of the file contains information about one firm. 
\item The first element of a line is the ID of a firm. The following elements of the line are the IDs of the sellers of inputs to the firm. 
\item The IDs in a line are integers greater than or equal to 1.
\item A network with n firms must have IDs from 1 to n.
\item The IDs in a line are comma seperated. 
\item Each firm in the network must have one and only one line in the file (even firms without any sellers of inputs must have a line).
\item The representative household's relations with the firms is Not reported in this file.
\item The fact that a firm may buy from itself is Not reported in this file.
\end{enumerate}
\subsection{Firm sector identification as \emph{firmSector.txt}}
This file contains information on the sector to which each firm belongs. 
\begin{enumerate}[I]
\itemsep-0.25em
\item Each line of the file contains information about one firm.
\item The first element of a line is the ID of a firm. The second element is the ID of the sector to which the firm belongs.
\item The IDs in a line are comma seperated. 
\item Each firm in the network must have one and only one line in the file.
\item The IDs of sectors are integers greater than or equal to 1.
\item An economy with s sectors must have sectoral IDs from 1 to s.
\end{enumerate}


\subsection{Household expenditure shares to different sectors as \emph{hhShares.txt}}
This file contains information on the shares of household expenditure on each sector of the economy.
\begin{enumerate}[I]
\itemsep-0.25em
\item Each line of the file contains information about on sector.
\item The first element of a line is the ID of a sector. The second element is the share of household expenditure on that sector.
\item Each sector of the economy must have  one and only one line in the file.
\item The sum of the shares of expenditure of the household on all sectors must be 1.
\end{enumerate}


\subsection{The distribution of money among firms \emph{money.txt}}
This file contains information on the distribution of money among firms in the economy.
\begin{enumerate}[I]
\itemsep-0.25em
\item Each line of the file contains information about one firm. 
\item The first element of a line is the ID of a firm. The second element is the money with the firm.
\item Each firm in the economy must have one and only one a line in the file.
\item The file does not specify the money with the representative household.
\end{enumerate}

\section{The code}
This sector describes the structure of Python code which takes as input information about an economic network, processing the data, inputs it into Cplex, and processes the output of esimates of matrix $A$ generates by Cplex.  The code is written in Python $2.7$ programming language. Before getting into the details of the code, it is worth noting that the overall objective of the code is to estimate matrix $A$  for a fixed network with different draws of the distribution of money of agents. With this objective in mind, the code incures a fixed cost of time in converting the economic network into various matrix and vectors which are written to disk. For every draw of money, the code then loads the matrixes and vectors from disk, combines with these the information on the distribution of money, and feeds these into Cplex, which then solves the problem. Our intention has been to minimize the time it takes to estimate $A$ for each draw of the distribution of money among agents. 
\\
\indent
With that in mind, the structure of the code is as follow. There are four files: \emph{inputProcess}, \emph{compute}, \emph{outputProcess}, and \emph{main}. They are all .py files as the code is written in Python. The \emph{inputProcess} file takes information on the linkages between firms in an economic network, and creates the various vectors and matrix Cplex needs to solve the problem, it then writes these vectors and matrix to disk. The \emph{compute} file loads information on the network from disk, combines these with information on the distribution of money, and loads all information into Cplex to solve the problem. The \emph{output} file processes the estimates of $A$ generates by Cplex by writing it into a txt file, generating various statistics and plots. 

\subsection{The file \emph{inputProcess.py}}
The file \emph{inputProcess.py} contains code to use data in \emph{network.txt}, \emph{firmSector.txt}, and \emph{hhShares.txt} to create and write to disk the matrix and vectors which Cplex needs to solve the optimization problem. Below we enumerate and explain each function in the file with examples. Each function is written in blue and the arguments in gray.

\begin{enumerate}[I]
\itemsep-0.25em
\item \textcolor{blue}{orderDictionary}(\textcolor{gray}{dictionary})
\\
This function takes one argument which is a dictionary. It converts the dictionary to an ordered dictionary, with the keys of the dictionary arragned in ascending order, and the values of each key arranged in ascending order.
\item \textcolor{blue}{createNetwork}(\textcolor{gray}{selfWeights})
\\
This function takes one argument which is a boolean. It loads \emph{network.txt} and generates a dictionary in which  IDs of agents are the keys. Each key has the  list of the IDS of its input sellers as the value. If the argument  \textcolor{gray}{selfWeights} is True, it adds each agent to the list of its input sellers. It adds the household as a seller of input to all firms. And it add the household as an agent which buys output from all firms. It calls the function  \textcolor{blue}{orderDictionary} to order the dictionary of network relations, and return dictionary of buyer-seller relations between all agents in the economy.
\item \textcolor{blue}{cplexNetwork}(\textcolor{gray}{selfWeights})
This function takes one argument which is a boolean. It calls  \textcolor{blue}{createNetwork} to generate the economic network in the form of a dictionary with the household, and with selfWeights if the boolean is True. It then write the economic network as \emph{cplexNetwork.txt}, with one line for each agent. Each line of \emph{cplexNetwork.txt} is comma separated with the first element being agent ID, and the following elements being the IDs of its input sellers. 
\item \textcolor{blue}{transform}(\textcolor{gray}{matchSector})
\\
This function takes one argument which is a boolean. The argument defines whether the optimization problem should have a constraint to match household spending shares to different sectors (constraint 3 in the earlier definition of the problem). The function generates several matrix and vectors later used by Cplex to solve the optimization problem.  The function creates these matrix and vectors using data from \emph{cplexNetwork.txt}. 
\\
Here we explaint the different vectors created by the function using a toy network. The toy network is as follows \{0:[1,2,3], 1:[0,2], 2:[0,3], 3:[0]\}. Agent 0 is the representative household. Agents 1, 2, and 3 are firms. The household buys goods from all three firms. Each firm buys labor from the household. Firm 1 buys goods from firm 2, and firm 2 buys from firm 3.
\\
TODO: details of the different lists and vectors it creates with the example.
\\
TODO: It calls the following functions to create the respective constraints
\item \textcolor{blue}{moneyEqualConstraint\_rowsCols}(\textcolor{gray}{inflows, indexes})
\\
The function takes two arguments: \emph{inflows} and \emph{indexes}, defined in the previous function. The function returns a tuple of two lists, which define the $Am=m$ constraint in the form $Mx=m$. The first list has the row indexes and the second list has the column indexes of the non-zero entries of matrix $M$.
\item \textcolor{blue}{weightsSumConstraint\_rowsCols}(\textcolor{gray}{outflows, indexes})
\\
The function takes two arguments: \emph{outflows} and \emph{indexes}. The function returns a tuple of two lists, which define the second constraint $ \sum_{j=0}^{n} a_{ij} = 1, \forall i \in (0,\ldots,n)$.  The constraint must be written as $Ex=e$, where $x$ is hte vector of unknowns. The first list has the row indexes and the second list has the column indexes of non-zero entries of matrix $E$.
\item \textcolor{blue}{hhSharesConstraint\_rowsColsVals}(\textcolor{gray}{outflows, indexes, sector\_indexes})
\\
The function takes three arguments: \emph{outflows}, \emph{indexes}, \emph{sector\_indexes}. It returns a tuple of two lists, which define the third constraint $(1-a_{00})(\sum_{j\in s} a_{0j}) = h_s, \forall s \in S $. The constraint must be written as $Hx=h$. The first list has the row indexes and the second list has the column indexes of non-zero entries of matrix $H$.
\item \textcolor{blue}{allConstraints\_rowsCols}(\textcolor{gray}{moneyEqual, weightsSum, hhSectoral, n, matchSector})
The functiont takes TODO. It combines the rows and column indexes of the matrix which charaterize the three constraints. More specifically, it stacks matrix $M, E,$ and $H$ and return the row and column indexes for non zero entries in the matrix that characterizes all constraints in Cplex.
\end{enumerate}

\subsection{The file \emph{compute.py}}
This file asks Cplex to solve the optimization problem


\end{document}
