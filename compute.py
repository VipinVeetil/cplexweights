from __future__ import division


import numpy as np
from scipy import optimize as opt
import collections
from numpy import *
import time
import cplex
from cplex.exceptions import CplexError
import sys
import timing
import copy
import matplotlib.pyplot as plt
from array import array
import os
import cjson
import json


def loadData(matchSector,selfWeight_zeroLB):
    directory_name = 'dataNetwork'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)
    with open("linearAgentSquare.txt", "r") as file:
        for line in file:
            linearAgentSquare = cjson.decode(line)
    with open("n.txt", 'r') as file:
        for line in file:
            n = cjson.decode(line)
    with open("k.txt", 'r') as file:
        for line in file:
            k = cjson.decode(line)
    with open("allConstraints.txt", 'r') as file:
        for line in file:
            allConstraints = cjson.decode(line)
    with open('inflows.txt', 'r') as file:
        for line in file:
            inflows = json.loads(line, object_pairs_hook=collections.OrderedDict)
    with open('obj.txt', 'r') as file:
        for line in file:
            obj = cjson.decode(line)
    with open('lowerBounds_base.txt', 'r') as file:
        for line in file:
            lowerBounds_base = json.loads(line, object_pairs_hook=collections.OrderedDict)
    with open('outflowsInverted_pairs.txt', 'r') as file:
        for line in file:
            outflowsInverted_pairs = json.loads(line, object_pairs_hook=collections.OrderedDict)
    with open('rhs_hhSectoral.txt', 'r') as file:
        for line in file:
            rhs_hhSectoral = cjson.decode(line)
    if selfWeight_zeroLB:
        with open('selfWeightsIndexes.txt', 'r') as file:
            for line in file:
                selfWeightsIndexes = json.loads(line)
    else:
        selfWeightsIndexes = False
    with open('selfWeightsObjective.txt', 'r') as file:
        for line in file:
            selfWeightsObjective = json.loads(line)
    print len(selfWeightsObjective), "length after loading"
    if matchSector:
        with open('hhSectoral_sharesConstraintVals.txt', 'r') as file:
            for line in file:
                hhSectoral_sharesConstraintVals = cjson.decode(line)
    else:
        hhSectoral_sharesConstraintVals = False

    os.chdir('..')
    returnDictionary = {'n': n, 'k': k,'inflows': inflows,
                        'lowerBounds_base': lowerBounds_base, 'allConstraints': allConstraints, 'obj': obj,
                        'outflowsInverted_pairs':outflowsInverted_pairs, 'rhs_hhSectoral':rhs_hhSectoral,
                        'hhSectoral_sharesConstraintVals':hhSectoral_sharesConstraintVals, 'selfWeightsIndexes':selfWeightsIndexes,
                        'linearAgentSquare':linearAgentSquare, 'selfWeightsObjective':selfWeightsObjective}
    return returnDictionary

def money():
    """converts money.txt to a dictionary"""
    moneyDictionary = collections.OrderedDict({})
    with open("money.txt") as money_file:
        for line in money_file:
            line = line.split(',')
            ID = int(line[0])
            money = np.float(line[1])
            moneyDictionary[ID] = money
    return moneyDictionary

def values_moneyEqualConstraint(moneyDictionary,inflows, outflowsInverted_pairs):
    values = []
    for ID in inflows:
        firmIndex = inflows[ID]
        for i in firmIndex:
            s = outflowsInverted_pairs[str(i)]
            m = moneyDictionary[s]
            values.append(m)
    return values

def constraint_values(moneyDictionary, inflows, outflowsInverted_pairs, k, hhSectoral_sharesConstraintVals):
    vals_moneyEqual = values_moneyEqualConstraint(moneyDictionary,inflows, outflowsInverted_pairs)
    vals_sumOne = [1] * k
    if hhSectoral_sharesConstraintVals:
        allValues =  vals_moneyEqual + vals_sumOne + hhSectoral_sharesConstraintVals
    else:
        allValues = vals_moneyEqual + vals_sumOne
    return allValues

def set_bounds(prob, k, lb_factor, lowerBounds_base, obj, selfWeights_factor,selfWeightsObjective,selfWeightsIndexes,
               eachAgentSquareDiff,linearAgentSquare ):
    my_ub = [1.0] * k



    if selfWeightsIndexes:
        selfWeightsIndexes = [str(i).decode('utf8') for i in selfWeightsIndexes]
        for ind in selfWeightsIndexes:
            lowerBounds_base[ind] = 0.0

    print len(lowerBounds_base), "number lb"

    if lb_factor:
        my_lb = lowerBounds_base.values()
        my_lb = [i / lb_factor for i in my_lb]
    else:
        print "zero lower bound"
        my_lb = [0.0] * k

    myvar = ['v' + str(i) for i in range(k)]

    obj = [selfWeights_factor * i for i in obj]
    #print obj[0:10], "before"
    if eachAgentSquareDiff:
        for i in xrange(len(linearAgentSquare)):
            term = linearAgentSquare[i]
            obj[i] += term

    #print obj[0:10], "after"
    #print obj, "obj
    #print my_ub, "my_ub"
    #print my_lb, "my_lb"
    #print myvar, "myvar"

    prob.variables.add(obj=obj, ub=my_ub, lb=my_lb, names=myvar)
    return prob

def set_linearConstraints(prob,n, rows_constraint, columns_constraint, moneyDictionary,
                          inflows, outflowsInverted_pairs,k,
                          rhs_hhSectoral,hhSectoral_sharesConstraintVals):


    vals = constraint_values(moneyDictionary=moneyDictionary, inflows=inflows,
                             outflowsInverted_pairs=outflowsInverted_pairs, k=k, hhSectoral_sharesConstraintVals=hhSectoral_sharesConstraintVals)
    IDS = moneyDictionary.keys()
    IDS.sort()
    my_rhs = []
    for ID in IDS:
        my_rhs.append(moneyDictionary[ID])
    rhs_sumWeights = [1] * n

    my_rhs += rhs_sumWeights

    my_sense  = "E" * n
    my_sense += my_sense


    if hhSectoral_sharesConstraintVals:
        my_rhs += rhs_hhSectoral
        number_sectors = len(rhs_hhSectoral)
        my_sense += "E" * number_sectors

    prob.linear_constraints.add(rhs=my_rhs, senses=my_sense)
    constraints = zip(rows_constraint, columns_constraint, vals)

    prob.linear_constraints.set_coefficients(constraints)

    return prob

def setCplex(prob, k, rows_constraint, columns_constraint, moneyDictionary,rhs_hhSectoral, hhSectoral_sharesConstraintVals,
             inflows, obj, n, lowerBounds_base,lb_factor, outflowsInverted_pairs,selfWeights_factor,
             quadraticObjetive, selfWeightsObjective,selfWeightsIndexes,eachAgentSquareDiff,linearAgentSquare):

    prob.objective.set_sense(prob.objective.sense.minimize)
    prob = set_bounds(prob=prob, k=k, lb_factor=lb_factor, lowerBounds_base=lowerBounds_base,
                      obj=obj, selfWeights_factor=selfWeights_factor, selfWeightsObjective=selfWeightsObjective,
                      selfWeightsIndexes=selfWeightsIndexes, eachAgentSquareDiff=eachAgentSquareDiff,linearAgentSquare =linearAgentSquare )

    prob = set_linearConstraints(prob=prob, rows_constraint=rows_constraint, columns_constraint=columns_constraint,
                                moneyDictionary=moneyDictionary, rhs_hhSectoral=rhs_hhSectoral, hhSectoral_sharesConstraintVals=hhSectoral_sharesConstraintVals,
                                 inflows=inflows, n=n, outflowsInverted_pairs=outflowsInverted_pairs,k=k)
    if quadraticObjetive:
        separable = [1.0] * k
        prob.objective.set_quadratic(separable)

    return prob

def solveCplex(lb_factor,selfWeights_factor,selfWeights,quadraticObjetive,selfWeightsObjective,
               matchSector, selfWeight_zeroLB, eachAgentSquareDiff):
    data = loadData(matchSector,selfWeight_zeroLB)
    print "loaded data"
    linearAgentSquare = data['linearAgentSquare']
    n = data['n']
    print n, "n"
    k = data['k'] # number of weights to estimate
    print k, "k"
    allConstraints = data['allConstraints']
    rows_constraint = allConstraints[0]
    columns_constraint = allConstraints[1]
    inflows = data['inflows']
    selfWeightsIndexes=data['selfWeightsIndexes']
    if selfWeights:
        obj = data['selfWeightsObjective'] # the objective function matrix, which sums the square each weight
    else:
        obj = None
    lowerBounds_base = data['lowerBounds_base']
    outflowsInverted_pairs = data['outflowsInverted_pairs']
    rhs_hhSectoral = data['rhs_hhSectoral']
    hhSectoral_sharesConstraintVals = data['hhSectoral_sharesConstraintVals']

    moneyDictionary = money()  # the distribution of money in the economy
    my_prob = cplex.Cplex()
    my_prob = setCplex(prob=my_prob, k=k, rows_constraint=rows_constraint,
                       columns_constraint=columns_constraint, moneyDictionary=moneyDictionary,
                       inflows=inflows, obj=obj, n=n, lowerBounds_base=lowerBounds_base, rhs_hhSectoral=rhs_hhSectoral,
                       hhSectoral_sharesConstraintVals=hhSectoral_sharesConstraintVals,
                       lb_factor=lb_factor, outflowsInverted_pairs=outflowsInverted_pairs,
                       selfWeights_factor=selfWeights_factor, eachAgentSquareDiff=eachAgentSquareDiff,
                       quadraticObjetive=quadraticObjetive, selfWeightsObjective=selfWeightsObjective,selfWeightsIndexes=selfWeightsIndexes,
                       linearAgentSquare=linearAgentSquare )
    print "created problem"
    #my_prob.parameters.qpmethod.set(4) # barrier method of solving the problem
    my_prob.solve()

    solution = my_prob.solution.get_values()
    return solution




