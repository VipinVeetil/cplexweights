from __future__ import division


import numpy as np
from scipy import optimize as opt
import collections
from numpy import *
import time
import cplex
from cplex.exceptions import CplexError
import sys
import timing
import copy
import matplotlib.pyplot as plt
from array import array
import os
import cjson
import json

def orderDictionary(dictionary):
    # takes one argument which is a dictionary. It converts the dictionary to an ordered dictionary, with the keys of
    #  the dictionary arragned in ascending order, and the values of each key arranged in ascending order.
    ordered_dictionary = collections.OrderedDict({})
    IDS = dictionary.keys()
    IDS.sort()
    for ID in IDS:
        inds = dictionary[ID]
        if type(inds) == 'list':
            inds.sort()
        ordered_dictionary[ID] = inds
    return ordered_dictionary

def createNetwork(selfWeights):
    # rewrite the network file so that it includes the household and selfWeights (if the selfWeights parameter is True)
    # add the household as supplier of input to all firms
    # add the household as a buyer of output from all firms

    networkDictionary = {}
    n = 0
    with open("network.txt", 'r') as file:
        for line in file:
            n += 1
    household_ID = 0
    print "number of firms", n

    with open("network.txt") as network:
        for l in network:
            l = l.split(',')
            l = [int(i) for i in l]
            firmID = l[0]
            suppliers = l[1:]
            if household_ID not in suppliers:  # add the household as a supplier
                suppliers.append(household_ID)
            if selfWeights:
                suppliers.append(firmID)
            suppliers = list(set(suppliers))  # set the list of suppliers to remove repeats
            networkDictionary[firmID] = suppliers
        allFirms_IDS = networkDictionary.keys()
        allFirms_IDS.sort()
        if selfWeights:
            all_agents = [0] + allFirms_IDS
            networkDictionary[household_ID] = all_agents
        else:
            networkDictionary[household_ID] = allFirms_IDS  # assign the household as a seller of goods to all firms

    return networkDictionary

def linear_objectiveFunction(n,k,indexes):
    V = np.zeros((1, k))
    count = 0
    for ID in xrange(0, n):
        ind = count * n + ID
        p = indexes.index(ind)
        V[0, p] = 1
        count += 1
    V = list(V[0])
    print len(V), "number of self weights"
    return V
def cplexNetwork(selfWeights):
    # loads network.txt which contains information about
    networkDictionary = createNetwork(selfWeights=selfWeights) # a dictionary agents as keys, and sellers as values
    network = orderDictionary(networkDictionary) # a dictionary with agents as keys, selles as values, ordered by ID
    with open("cplexNetwork.txt", 'w') as Cplex_network:
        for ID in network:
            sellers = network[ID]
            sellers.insert(0,ID) # insert the ID of agent in the first position
            line = sellers
            line = ','.join(map(str, line))
            Cplex_network.write(line + '\n')

def transform(matchSector,selfWeights):
    # this function transforms network linkages to data which can be loaded and used by Cplex in solving the linear programming problem
    linearAgentSquare = []
    selfWeightsObjective = []
    iN_list_row = []
    jN_list_column = []
    indexes = []
    outflows = collections.OrderedDict({})
    inflows = collections.OrderedDict({})
    lowerBounds_base = collections.OrderedDict({})
    outflowsInverted_pairs = collections.OrderedDict({})
    sector_indexes = collections.OrderedDict({})
    firms_sector = {}
    selfWeightsIndexes = []
    with open('firmSector.txt', 'r') as file:
        for line in file:
            line = line.split(",")
            firm = int(line[0])
            sector = int(line[1])
            firms_sector[firm] = sector

    all_sectors = list(set(firms_sector.values()))
    all_sectors.sort()
    for s in all_sectors:
        sector_indexes[s] = []
    n = sum(1 for line in open("cplexNetwork.txt"))
    with open('cplexNetwork.txt') as network:
        count = 0
        for line in network:
            line = line.split(',')
            line = [int(i) for i in line]
            firmID = line[0]
            outflows[firmID] = []
            suppliers = line[1:]
            lb = 1 / len(suppliers)
            suppliers.sort()
            number = -2 / len(suppliers)
            linearAgentSquare.append(number)
            for s in suppliers:
                if firmID == s:
                    selfWeightsObjective.append(1)
                else:
                    selfWeightsObjective.append(0)
                ind = s + count * n
                if s != 0:
                    sec = firms_sector[s]
                    sector_indexes[sec].append(ind)
                outflows[firmID].append(ind)
                indexes.append(ind)
                iN_list_row.append(count)
                jN_list_column.append(s)
                outflowsInverted_pairs[ind] = firmID
                if s not in inflows.keys():
                    inflows[s] = []
                inflows[s].append(ind)
                lowerBounds_base[ind] = lb
                if s == firmID:
                    selfWeightsIndexes.append(ind)
            count += 1
            if count % 10000 == 0:
                print count
    inflows = orderDictionary(inflows)

    sectoralShares = {}
    with open('hh_sectoralShares.txt', 'r') as file:
        for line in file:
            line = line.split(",")
            sector = int(line[0])
            share = float(line[1])
            sectoralShares[sector] = share
    sectoralShares = orderDictionary(sectoralShares)
    rhs_hhSectoral = sectoralShares.values()

    moneyEqualConstraint = moneyEqualConstraint_rowsCols(inflows=inflows, indexes=indexes)
    print "made money equal constraint"

    weightsSumConstraint = weightsSumConstraint_rowsCols(outflows=outflows, indexes=indexes)
    print "made weights sum one constraint"

    if matchSector:
        hhSectoral_sharesConstraint = hhSharesConstraint_rowsColsVals(outflows, indexes,sector_indexes)
        hhSectoral_sharesConstraintVals = hhSectoral_sharesConstraint[2]
        print "made hhSectoral share constraint"
        allConstraints = allConstraints_rowsCols(moneyEqual=moneyEqualConstraint, weightsSum=weightsSumConstraint,
                                                 hhSectoral=hhSectoral_sharesConstraint[:-1], n=n,matchSector=matchSector)
    else:
        allConstraints = allConstraints_rowsCols(moneyEqual=moneyEqualConstraint, weightsSum=weightsSumConstraint,
                                                 hhSectoral=None, n=n,matchSector=matchSector)

    k = len(indexes)
    print k, "number of weights"



    if not matchSector:
        hhSectoral_sharesConstraintVals = None


    if selfWeights:
        obj = linear_objectiveFunction(n, k, indexes)
    else:
        obj = None

    print "writing stage len", len(selfWeightsObjective)
    write(iN_list_row=iN_list_row, jN_list_column=jN_list_column,
          indexes=indexes, allConstraints=allConstraints, k=k,
          n=n, obj=obj,lowerBounds_base=lowerBounds_base, inflows=inflows,
          outflowsInverted_pairs=outflowsInverted_pairs, rhs_hhSectoral=rhs_hhSectoral,
          hhSectoral_sharesConstraintVals=hhSectoral_sharesConstraintVals, selfWeightsIndexes=selfWeightsIndexes,
          linearAgentSquare=linearAgentSquare,selfWeightsObjective=selfWeightsObjective)

def moneyEqualConstraint_rowsCols(inflows,indexes):
    # The function takes two arguments: inflows and indexes. The function returns a tuple of two lists, which define
    # the Am=m constraint in the form Mx=m. The first list has the row indexes and the second list has the column
    # indexes of the non-zero entries of matrix M.
    moneyEqualConstraint_rows = []
    moneyEqualConstraint_cols = []
    for ID in inflows:
        firmIndex = inflows[ID]
        for i in firmIndex:
            p = indexes.index(i)
            moneyEqualConstraint_rows.append(ID)
            moneyEqualConstraint_cols.append(p)
    moneyEqualConstraint = (moneyEqualConstraint_rows, moneyEqualConstraint_cols)
    return moneyEqualConstraint

def weightsSumConstraint_rowsCols(outflows, indexes):
    #The function takes two arguments: outflows and indexes.
    # The function returns a tuple of two lists, which define the second constraint that weights of each agent must sum to 1.
    # The constraint must be written as Ex=e, where x is the vector of unknowns.
    # The first list has the row indexes and the second list has the column indexes of non-zero entries of matrix E
    rows = []
    columns = []
    for ID in outflows:
        firmIndex = outflows[ID]
        ID = int(ID)
        for i in firmIndex:
            p = indexes.index(i)
            rows.append(ID)
            columns.append(p)
    weightsSumConstraint = (rows, columns)
    return weightsSumConstraint

def hhSharesConstraint_rowsColsVals(outflows,indexes,sector_indexes):
    # It returns a tuple of two lists, which define the third constraint that household shares of expenditure is matched
    # The constraint must be written as Hx=h.
    # The first list has the row indexes and the second list has the column indexes of non-zero entries of matrix H.
    sectoralShares = {}
    with open('hh_sectoralShares.txt', 'r') as file:
        for line in file:
            line = line.split(",")
            sector = int(line[0])
            share = float(line[1])
            sectoralShares[sector] = share
    # we need to write rows and columns of matrix S, such S x = b,
            # where x is a k times 1 vector of unknown weights, b is a s times 1 vector of shares of the hh spending by sector
            # and S is s times k, with s as the number of sectors; note S has zeros for all k which does not relate to money going out of the household
    rows = []
    columns = []
    vals = []
    sectors = sectoralShares.keys()
    sectors.sort()

    #one row for each sector
    hh_inds = set(outflows[0])
    count = 0
    for s in sectors:
        # for each link out of the household going to that sector, add a row count, and column is the position of that link in indexes
        sec_inds = sector_indexes[s]
        hh_sec_inds = list(set(sec_inds).intersection(hh_inds))
        hh_sec_inds.sort()
        rows.append(count)
        columns.append(0)
        share =  sectoralShares[s]
        vals.append(share)
        for ind in hh_sec_inds:
            rows.append(count)
            p = indexes.index(ind)
            columns.append(p)
            vals.append(1)
        count += 1
    hh_shareConstraint = (rows, columns, vals)
    return hh_shareConstraint

def allConstraints_rowsCols(moneyEqual, weightsSum, hhSectoral, n, matchSector):


    rows_moneyEqual = moneyEqual[0]
    cols_moneyEqual = moneyEqual[1]
    rows_sumOne = weightsSum[0]
    cols_sumOne = weightsSum[1]
    if matchSector:
        rows_hhSectoral = hhSectoral[0]
        cols_hhSectoral = hhSectoral[1]

    adjusted_rows_sumOne = [i + n for i in rows_sumOne]
    if matchSector:
        adjusted_rows_hhSectoral = [i+2*n for i in rows_hhSectoral]
        allRows = rows_moneyEqual + adjusted_rows_sumOne + adjusted_rows_hhSectoral
        allColumns = cols_moneyEqual + cols_sumOne + cols_hhSectoral  # we donot add W2 columns because matrix is
        # stacked on top of each other

    else:
        allRows = rows_moneyEqual + adjusted_rows_sumOne
        allColumns = cols_moneyEqual + cols_sumOne

    return allRows, allColumns

def write(iN_list_row, jN_list_column, indexes, allConstraints, k, n,
          obj,lowerBounds_base, inflows,outflowsInverted_pairs,rhs_hhSectoral,
          hhSectoral_sharesConstraintVals,selfWeightsIndexes,linearAgentSquare,selfWeightsObjective):

    directory_name = 'dataNetwork'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)
    with open('ijIndexes.txt', 'w') as ijInd:
        for t in xrange(k):
            i = iN_list_row[t]
            j = jN_list_column[t]
            v = indexes[t]
            ls = [i, j, v]
            ls = ','.join(map(str, ls))
            ijInd.write(ls + '\n')
    with open('allConstraints.txt', 'w') as file_allConstraints:
        file_allConstraints.write(cjson.encode(allConstraints))
    with open('k.txt', 'w') as file_k:
        file_k.write(cjson.encode(k))
    with open('n.txt', 'w') as file_n:
        file_n.write(cjson.encode(n))
    with open('lowerBounds_base.txt', 'w') as file_lowerBounds_base:
        file_lowerBounds_base.write(json.dumps(lowerBounds_base))
    with open('obj.txt', 'w') as file_obj:
        file_obj.write(cjson.encode(obj))
    with open('inflows.txt', 'w') as file_inflows:
        file_inflows.write(json.dumps(inflows))
    with open('outflowsInverted_pairs.txt', 'w') as file_outflowsInverted_pairs:
        file_outflowsInverted_pairs.write(json.dumps(outflowsInverted_pairs))
    with open('rhs_hhSectoral.txt', 'w') as file_rhs_hhSectoral:
        file_rhs_hhSectoral.write(cjson.encode(rhs_hhSectoral))
    with open('hhSectoral_sharesConstraintVals.txt', 'w') as file_hhSectoral_sharesConstraintVals:
        file_hhSectoral_sharesConstraintVals.write(cjson.encode(hhSectoral_sharesConstraintVals))
    with open('selfWeightsIndexes.txt', 'w') as file_selfWeightsIndexes:
        file_selfWeightsIndexes.write(json.dumps(selfWeightsIndexes))
    with open('linearAgentSquare.txt', 'w') as file_linearAgentSquare:
        file_linearAgentSquare.write(json.dumps(linearAgentSquare))
    with open('selfWeightsObjective.txt', 'w') as file_selfWeightsObjective:
        file_selfWeightsObjective.write(json.dumps(selfWeightsObjective))
        print "jason stage", len(selfWeightsObjective)
    os.chdir('..')



def writeMoney(randomMoney):
    # writes a money file, with each agent having as much money as the number of its sellers
    # this is merely for testing purposes, actual estimation needs to load a file with actual money
    network = {}
    with open('Cplex_network.txt', 'r') as networkFile:
        for line in networkFile:
            line = line.split(",")
            line = [int(i) for i in line]
            firm = line[0]
            sellers = line[1:]
            network[firm] = sellers

    moneyDict = collections.OrderedDict({})
    IDS_ALL = network.keys()
    More_IDS = network.values()
    for more in More_IDS:
        IDS_ALL += more
    IDS_ALL = list(set(IDS_ALL))
    IDS_ALL.sort()
    count = 0
    for l in IDS_ALL:
        nSellers = len(network[l])
        if randomMoney:
            moneyDict[l] = random.uniform(0, 1) * 10
        else:
            moneyDict[l] = nSellers + 0.1 * count
            if count < 10:
                count += 1
            else:
                count = 0

    moneys = moneyDict.values()
    moneys = moneys[1:]
    firms_money = sum(moneys)
    moneyDict[0] =  0.5 * firms_money

    with open('money.txt', 'w') as moneyFile:
        for ID in moneyDict:
            money = moneyDict[ID]
            m = [ID, money]
            m = ','.join(map(str, m))
            moneyFile.write(m + '\n')
            if count < 10:
                count += 1
            else:
                count = 0
