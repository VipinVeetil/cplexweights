from __future__ import division


import numpy as np
from scipy import optimize as opt
import collections
from numpy import *
import time
import cplex
from cplex.exceptions import CplexError
import sys
import timing
import copy
import matplotlib.pyplot as plt
from array import array
import inputProcess
import outputProcess
import compute


def main(processNetwork, writeMoney,lb_factor,selfWeights_factor,selfWeights,quadraticObjetive,
         selfWeightsObjective,selfWeight_zeroLB,randomMoney,matchSector,eachAgentSquareDiff):
    if processNetwork:
        inputProcess.cplexNetwork(selfWeights=selfWeights)
        print "Cplex network"
        inputProcess.transform(matchSector=matchSector,selfWeights=selfWeights)
        print "transform"
    if writeMoney:
        inputProcess.writeMoney(randomMoney)
        print "wrote money"
    solutionValues = compute.solveCplex(lb_factor,selfWeights_factor,selfWeights,
                                        quadraticObjetive,selfWeightsObjective,matchSector,selfWeight_zeroLB,
                                        eachAgentSquareDiff)
    outputProcess.writeWeights(weights=solutionValues)
    outputProcess.writeWeightsLinks()
    outputProcess.testInvariance_plot()


main(processNetwork=False, writeMoney=False, lb_factor=100000000, selfWeights_factor=1, selfWeights=True,
     quadraticObjetive=True, selfWeightsObjective=True,selfWeight_zeroLB=True,randomMoney=False,
     matchSector=False,eachAgentSquareDiff=False)
