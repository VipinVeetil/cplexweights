from __future__ import division
import numpy as np
from scipy import optimize as opt
from itertools import izip
import collections
from numpy import *
import time
import cplex
from cplex.exceptions import CplexError
import sys
import timing
import copy
import matplotlib.pyplot as plt
from array import array
import os


def writeWeightsLinks():# write the estimated weights along with the links to which the weights pertain
    weights = []
    directory_name = 'dataNetwork'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)

    with open("weightsOnly.txt", 'r') as file:
        for line in file:
            w = float(line)
            weights.append(w)

    indexes = []
    with open("ijIndexes.txt", 'r') as file:
        for line in file:
            line = line.split(",")
            line = [int(i) for i in line]
            buyer = line[0]
            seller = line[1]
            pair = (buyer,seller)
            indexes.append(pair)
    weightsPairs = collections.OrderedDict({})
    count = 0
    for pair in indexes:
        w = weights[count]
        weightsPairs[pair] = w
        count += 1
    os.chdir('..')
    with open("weights.txt", 'w') as file:
        for pair in weightsPairs:
            w = weightsPairs[pair]
            info = list(pair) + [w]
            info = ','.join(map(str, info))
            file.write(info + '\n')


def writeWeights(weights): # write the estimated weights
    directory_name = 'dataNetwork'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)
    with open("weightsOnly.txt", 'w') as file:
        for w in weights:
            file.write(str(w) + '\n')
    os.chdir('..')



def testInvariance_plot(): # test the invariance of the size distribution which comes out of the estimated weights
    # and plot the CDF of the estimated weights
    moneyDictionary = collections.OrderedDict({})
    with open("money.txt", "r") as file: # load the initial distribution of money
        for line in file:
            line = line.split(",")
            ID = int(line[0])
            m = float(line[1])
            moneyDictionary[ID] = m


    oldMoney = moneyDictionary.values()
    n = len(moneyDictionary.keys())
    A = np.zeros((n, n))

    selfWeights = []
    firm_otherWeights = []
    hh_weights = []
    allWeights = []
    directory_name = 'dataNetwork'
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    os.chdir(directory_name)
    with open('ijIndexes.txt', 'r') as indexesFile, open('weightsOnly.txt', 'r') as weightsFile:
        for indexes, weight in izip(indexesFile,weightsFile):
            indexes = indexes.split(",")
            i = int(indexes[0])
            j = int(indexes[1])
            weight = float(weight)
            A[i, j] = weight
            if i == j:
                selfWeights.append(weight)
            else:
                if i != 0:
                    firm_otherWeights.append(weight)
            if i == 0:
                hh_weights.append(weight)
            allWeights.append(weight)
    os.chdir('..')



    mean_selfWeight = sum(selfWeights)/len(selfWeights)
    print max(selfWeights), "max self weight"
    print min(selfWeights), "min self weight"
    print mean_selfWeight, "mean self weight"
    meanWeights = sum(allWeights)/len(allWeights)
    print meanWeights, "mean weight"



    newMoney = np.dot(np.transpose(A), oldMoney)


    diff_list = []
    for i in xrange(len(newMoney)):
        new = newMoney[i]
        old = oldMoney[i]
        diff = abs(old - new)
        diff_list.append(diff/old)

    print np.mean(diff_list), "mean money difference" # mean different between initial sizes and sizes computed with estimated weights
    print max(diff_list), "max money difference"
    print min(diff_list), "min money difference"


    nM = list(newMoney)
    nM = [round(i,1) for i in nM]

    print oldMoney[0:10], "old money" # first few entries of old sizes
    print nM[0:10], "new money" # first few entries of sizes computed with estimated weights

    print sum(oldMoney), "sum Old Money" # sum of old money
    print sum(nM), "sum New Money" # sum of money after dynamics with estimated weights


    weightsDistribution_bySize = collections.OrderedDict({})
    selfWeightsDistribution_bySize = collections.OrderedDict({})
    firm_otherWeightsDistribution_bySize = collections.OrderedDict({})
    hh_WeightsDistribution_bySize  = collections.OrderedDict({})

    for i in xrange(0,15):
        weightsDistribution_bySize[i] = 0
        selfWeightsDistribution_bySize[i] = 0
        firm_otherWeightsDistribution_bySize[i] = 0
        hh_WeightsDistribution_bySize[i] = 0

    for i in xrange(0, 15):
        count = 0
        for w in allWeights:
            if w < 10 ** -i:
                count += 1
        weightsDistribution_bySize[i] = count
        count = 0
        for w_self in selfWeights:
            if w_self < 10 ** -i:
                count += 1
        selfWeightsDistribution_bySize[i] = count
        count = 0
        for w_firm in firm_otherWeights:
            if w_firm < 10 ** -i:
                count += 1
        firm_otherWeightsDistribution_bySize[i] = count
        count = 0
        for w_hh in hh_weights:
            if w_hh < 10 ** -i:
                count += 1
        hh_WeightsDistribution_bySize[i] = count


    n = len(allWeights)
    y = weightsDistribution_bySize.values()
    W_y = [i/n for i in y]
    W_x = weightsDistribution_bySize.keys()

    n = len(selfWeights)
    y = selfWeightsDistribution_bySize.values()
    selfW_y = [i/n for i in y]
    selfW_x = selfWeightsDistribution_bySize.keys()

    n = len(firm_otherWeights)
    y = firm_otherWeightsDistribution_bySize.values()
    firmW_y = [i / n for i in y]
    firmW_x = firm_otherWeightsDistribution_bySize.keys()

    n = len(hh_weights)
    y = hh_WeightsDistribution_bySize.values()
    hhW_y = [i / n for i in y]
    hhW_x = hh_WeightsDistribution_bySize.keys()




    plt.xlabel("10 to the negative ")
    plt.ylabel("Proportion of weights")
    plt.title("Proportion of weights less than 10 to power negative")
    plt.grid()
    plt.savefig('weightsSizeScatter.png')
    plt.close()

    selfW_x = [-i for i in selfW_x]
    firmW_x = [-i for i in firmW_x]
    hhW_x = [-i for i in hhW_x]
    plt.plot(selfW_x, selfW_y, color='blue', label='Self-weights', linestyle='--', lw=2)
    plt.plot(firmW_x, firmW_y, color='black', label='Inter firm weights', lw=2)
    plt.plot(hhW_x, hhW_y, color='red', label='Consumption weights', linestyle='-.', lw=1.5)
    plt.legend(loc=2)
    plt.suptitle("CDF of production network weights", fontsize=20)
    plt.xlabel("Size (log base 10)",fontsize=16)
    plt.ylabel("Proportion of weights",fontsize=16)
    plt.grid()
    plt.savefig('weightsSize.png')
    plt.close()





